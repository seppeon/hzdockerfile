FROM archlinux:latest

RUN pacman --noconfirm -Syu
RUN pacman --noconfirm -S \
    wget \
    base-devel \
    pkgconf \
    libglvnd \
    libfontenc \
    libice \
    libsm \
    libxaw \
    libxcomposite \
    libxcursor \
    libxtst \
    libxinerama \
    libxkbfile \
    libxrandr \
    libxres \
    libxss \
    libxvmc \
    xcb-util-wm \
    xcb-util-keysyms \
    libxv \
    xcb-util-cursor \
    xkeyboard-config \
    mingw-w64-gcc \
    gcc \
    cmake \
    clang \
    ninja \
    python \
    python-pip \
    python-virtualenv \
    nodejs \
    npm \
    nano \
    git

RUN virtualenv /hz_venv
RUN ls /hz_venv
RUN . /hz_venv/bin/activate && pip install conan

